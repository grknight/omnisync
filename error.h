#ifndef ERROR_H
#define ERROR_H

#include <assert.h>
#define CHECK(x) assert((x)>=0)
void error_exit(char *format, ...);

#endif	// ERROR_H

