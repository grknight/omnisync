#ifndef MSSL_GNUTLS_H
#define MSSL_GNUTLS_H

int READ_SSL(gnutls_session_t ssl_h, char *whereto, int len);
int WRITE_SSL(gnutls_session_t ssl_h, char *whereto, int len);
int connect_ssl(int socket_h, gnutls_session_t session, char *hostname);
void initialize_ctx(void);

extern gnutls_certificate_credentials_t gnutls_xcred;
extern gnutls_datum_t gnutls_session_data;
extern int https_resume;

#endif	// MSSL_GNUTLS_H

