#ifndef HTTP_H
#define HTTP_H

int httptime(char *bind_to, char *host, int host_port, double timeout, char *proxy, int proxy_port, char mode,
#ifndef WITH_GNUTLS
 SSL_CTX *ctx,
#endif
 double *ts_start_recv, double *ts);
#endif	// HTTP_H

